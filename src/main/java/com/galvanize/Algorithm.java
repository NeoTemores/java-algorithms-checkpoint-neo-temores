package com.galvanize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Algorithm {

    public boolean allEqual(String word){
        if(word.length() == 0) return false;

        String lowerCaseWord = word.toLowerCase();
        char firstLetter = lowerCaseWord.charAt(0);

        for(int i = 1; i < lowerCaseWord.length(); i++){
            if(lowerCaseWord.charAt(i) != firstLetter){
                return false;
            }
        }
        return true;
    }

    public Map <String, Long> letterCount(String word){
        Map<String, Long> result = new HashMap<>();
        String lowerCaseWord = word.toLowerCase();

        for(int i = 0; i < lowerCaseWord.length(); i++){

            String currentLetter = String.valueOf(lowerCaseWord.charAt(i));

            if(!result.containsKey(currentLetter)){
                result.put(currentLetter, 1l);
            } else {
                result.put(currentLetter, result.get(currentLetter) + 1);
            }
        }

        return result;
    }

    public String interleave(List<String> list1, List<String> list2){
        StringBuilder result = new StringBuilder();

       for(int i = 0; i < list1.size(); i++){
           result.append(list1.get(i));
           result.append(list2.get(i));
       }

        return result.toString();
    }

}
