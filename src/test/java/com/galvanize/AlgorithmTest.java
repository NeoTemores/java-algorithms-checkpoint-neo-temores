package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AlgorithmTest {

    private Algorithm algorithm;
    @BeforeEach
    void setUp(){
    algorithm = new Algorithm();
    }

    @Test
    void testAllEqual(){
        String word1 = "aAa"; //true
        String word2 = "bbBbabbb"; //false
        String word3 = ""; //false

        boolean expected1 = true;
        boolean expected2 = false;
        boolean expected3 = false;

        boolean actual1 = algorithm.allEqual(word1);
        boolean actual2 = algorithm.allEqual(word2);
        boolean actual3 = algorithm.allEqual(word3);

        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        assertEquals(expected3, actual3);
        }


    @Test
    void testLetterCount(){
        String word1 = "aa";
        String word2 = "abBcd";
        String word3 = "";

        Map<String, Long> expected1 = new HashMap<>();
        expected1.put("a", 2l);
        Map<String, Long> expected2 = new HashMap<>();
        expected2.put("a", 1l);
        expected2.put("b", 2l);
        expected2.put("c", 1l);
        expected2.put("d", 1l);
        Map<String, Long> expected3 = new HashMap<>();

        Map<String, Long> actual1 = algorithm.letterCount(word1);
        Map<String, Long> actual2 = algorithm.letterCount(word2);
        Map<String, Long> actual3 = algorithm.letterCount(word3);

        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        assertEquals(expected3, actual3);
    }

    @Test
    void testInterleave(){
        List<String> list1 = new ArrayList<>(Arrays.asList("a", "b", "c"));
        List<String> list2 = new ArrayList<>(Arrays.asList("d", "e", "f"));

        String expected1 = "adbecf";
        String expected2 = "";

        String actual1 = algorithm.interleave(list1, list2);
        String actual2 = algorithm.interleave(Collections.emptyList(), Collections.emptyList());

        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);




    }

}
